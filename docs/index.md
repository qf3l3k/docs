---
sidebar_position: 1
---

# Introduction


## What is the purpose of the Dyson Protocol?

Dyson Protocol is an innovative blockchain network designed to change the way decentralized web applications (dApps) - or "DWApps" - are created and deployed. By harnessing the power of Python - one of the world's most popular programming languages - Dyson Protocol makes Web3 dApp development more accessible and user-friendly than ever before.

TL;DR
- written in Python
- frontend on-chain
- automatic UI generation
- full HTTP control

## The Purpose of the Dyson Protocol

### Lower the barrier of entry 
The primary purpose of the Dyson Protocol is twofold. Firstly, it aims to lower the barrier of entry for developers in creating distributed web apps by leveraging the familiarity and versatility of **Python**, an already popular language in the programming community. With Dyson Protocol, developers can utilize their existing knowledge of Python syntax and standard libraries to write smart contracts or scripts, enabling them to leverage their skills and expertise seamlessly.

### Eliminating Single Points of Failure
The Dyson Protocol aims to eliminate a major single point of failure in the crypto ecosystem – the user interface. By hosting the **frontend directly on-chain**, the Dyson Protocol ensures that the user interface becomes an integral part of the blockchain network, increasing its resilience and reducing potential vulnerabilities associated with centralized hosting.

### Python: Familiarity and Efficiency Combined
Scripts, also known as smart contracts, within the Dyson Protocol are written in a secure <a href="/supported-python-syntax">subset of Python</a> called Dyslang. This subset allows developers to leverage their existing knowledge of Python syntax and <a href="available-python-functions">standard libraries</a>, making the development process more efficient and straightforward. By eliminating the need to learn a new programming language specifically for dWApp development, Dyson Protocol empowers developers to focus on building innovative applications without unnecessary barriers.

### Automatic UI Generation: Simplifying User Interactions
One of the notable features of Dyson Protocol is its ability to **automatically generate user interfaces (UIs)** for script functions. When developers define functions within their scripts, these definitions are automatically parsed into json-schema and rendered as HTML forms in the dashboard. This automated UI generation eliminates the need for developers to learn additional languages like JavaScript or Protobufs, or to directly interact with wallets. Users can now interact with the script through the generated UI without requiring any specialized knowledge, further enhancing accessibility and ease of use.

### Full HTTP Control: Unleashing the Power of WSGI
Dyson Protocol utilizes the widely adopted <a href="https://wsgi.readthedocs.io/en/latest/learn.html">WSGI protocol</a> , providing scripts with full control over HTTP requests and responses. By creating the appropriate `application()` function within scripts, developers gain access to the raw HTTP request and can exert **complete control over the HTTP** response sent to the client. This level of control empowers developers to design dynamic and customized interactions between dWApps and users, facilitating the creation of more sophisticated and responsive applications.

### Embracing Open Source and Collaboration
Dyson Protocol takes an open-source approach, fostering collaboration, trust, and innovation within the wider community. All user scripts written in Dyslang are **inherently open source**, allowing users to access and interact with dWApps through regular web URLs. Furthermore, users can view the source code of each script using a unique script address, promoting transparency and enabling scrutiny of the underlying code.

By adopting this "open source only" philosophy, Dyson Protocol encourages developers to learn from one another, adapt existing code for their own needs, and contribute improvements back to the community. In turn, users benefit from increased transparency, as they can examine the source code of the dWApps they interact with, ensuring the expected functionality and providing confidence in data handling.

In summary, Dyson Protocol not only leverages the power and familiarity of Python to streamline dWApp development but also embraces an open-source ethos to create a decentralized ecosystem


## How to get support or report bugs?

Head over to [GitHub](https://github.com/orgs/dysonprotocol/discussions) and drop a comment.

Or join [Discord](https://discord.gg/FZfKmSJCyP), verify your account, and ask for help there.
